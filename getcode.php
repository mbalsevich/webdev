<?php
/**
 *
 * getcode.php
 *
 * Este archivito hicimos para que todos puedan copiar
 * el contenido de una página porque aún no teníamos un
 * repositorio de código.
 *
 * Está deprecado, pero queda como historial.
 *
 */

define('EL_ARCHIVO','getpage_curl.php');

// Respuesta es texto, no html
header('Content-type:text/plain');


// Abrir archivo
$f=fopen(EL_ARCHIVO,'rb');
if( !$f )die('No puedo abrir el archivo: '.EL_ARCHIVO);

// Leer e imprimir el contenido
fpassthru($f);

// Cerrar el archivo
fclose($f);


?>