<?php
/**
 *
 * getpage.php
 *
 * Este archivito hicimos para que todos puedan copiar
 * el contenido de una página porque aún no teníamos un
 * repositorio de código.
 *
 * Está deprecado, pero queda como historial.
 *
 */
define('URL_PAGINA','http://www.apachefriends.org/en/index.html');
define('URL_IMAGEN','http://www.apachefriends.org/images/211.jpg');


$prueba = intval( @$_REQUEST['prueba'] );

if( !$prueba )$prueba = 1;

switch( $prueba )
{
default:
case 1:
	$url = URL_PAGINA;
	$content_type = null;
	break;

case 2:
	$url = URL_PAGINA;
	$content_type = 'text/plain';
	break;

case 3:
	$url = URL_IMAGEN;
	$content_type = null;
	break;

case 4:
	$url = URL_IMAGEN;
	$content_type = 'image/jpg';
	break;
}


// Abrir el URL
$file = fopen( $url ,'rb');
if( !$file )die('No puedo abrir el URL '.$url);

// Leer todo los datos y guardarlos en una variable
$data = stream_get_contents($file);

// Cerrar el URL
fclose( $file );


// Imprimir el tipo (si se especifica)
if( $content_type )
{
	header('Content-type: '.$content_type);
}

// Imprimir el contenido
echo $data;

?>