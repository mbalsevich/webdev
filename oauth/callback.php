<?php

/*
 *
 * Esta es la página a la que redirigimos al usuario
 * luego de una interacción. Tenemos que procesar el resultado
 *
 * Debe ser incluída luego de definir la constante 'EL_SERVICIO';
 *
 */

if( !defined('EL_SERVICIO')){
	die('Servicio no ha sido definido');
}

$servicio = EL_SERVICIO;
$cliente = new ClienteOAuth();


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>UCA WebDev: <?php echo $cliente->GetNombreDelServicio($servicio); ?> API</title>
	<link rel="stylesheet" type="text/css" href="./webdev.css" />
</head>
<body>
	<div class="contenido"><?php


	$codigo_autorizacion =@$_REQUEST['code'];

	if( $codigo_autorizacion )
	{
		ProcesarCodigo( $cliente, $servicio, $codigo_autorizacion );

	}
	else
	{
		ProcesarError($cliente, $servicio );
	}
	?></div>
	<div class="pie_pagina">
		<a href="./">Inicio</a>
	</div>
</body>
</html><?php

function ProcesarCodigo(ClienteOAuth $cliente, $servicio, $codigo_autorizacion)
{
?><div class="cuadro_aviso">
	<div class="titulo">Codigo obtenido</div>
	<div class="cuerpo"><?php
		echo '<b>Código:</b> ',$codigo_autorizacion,'<br>';

		if( $cliente->ObtenerTokenDeAcceso($servicio, $codigo_autorizacion) )
		{
			echo '<b>Token de Accesso:</b> ',$cliente->GetTokenDeAcceso($servicio)
					,'<div style="margin:5px;text-align:center"><a href="./?servicio='.$servicio.'&accion=listo">Retornar al inicio</a></div>';
		}
		else
		{
			echo '<b>Error:</b> ',$cliente->GetUltimoError()
					,'<br>Por favor, <a href="',$cliente->GetUrlParaAutorizarApp($servicio)
					,'">reintente</a>';
		}
	?></div>
</div><?php
}

function ProcesarError(ClienteOAuth $cliente, $servicio)
{
?><div class="cuadro_aviso">
	<div class="titulo">ERROR</div>
	<div class="cuerpo"><?php
		echo 'El servidor no proveyó el código de autorización.<br>';
		$error = @$_REQUEST['error'];
		if( $error ){
			echo '<b>Error:</b> ',$error,'<br>';
		}
		echo 'Por favor, <a href="',$cliente->GetUrlParaAutorizarApp($servicio),'">reintente</a>';
	?></div>
</div><?php
}

?>