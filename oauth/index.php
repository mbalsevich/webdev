<?php
require_once('lib/cliente-oauth.php');

define('SERVICIO_SOLICITADO',@$_REQUEST['servicio']);
define('ACCION_SOLICITADA',@$_REQUEST['accion']);

$cliente = new ClienteOAuth();



?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>UCA WebDev: Google API</title>
	<link rel="stylesheet" type="text/css" href="./webdev.css" />
</head>
<body>
	<div class="contenido"><?php

	
	$servicios = $cliente->GetListaDeServicios();
	foreach( $servicios as $servicio=>$nombre )
	{
		ImprimirEstadoServicio( $cliente, $servicio, $nombre );
	}
	?></div>
</body>
</html><?php

function ProcesarAccion(ClienteOAuth $cliente, $servicio, $accion )
{
	echo '<div>Acción: <b>',$accion,'</b><div style="margin:0 0 0 5px">';
	switch( $accion )
	{
	case 'olvidar':
		$cliente->OlvidarTokenDeAcceso( $servicio );
		echo 'Token olvidado';
		break;
	
	case 'error':
		echo @$_REQUEST['error'];
		break;

	case 'listo':
		echo 'Conectado OK';
		break;

	case 'videos':
		ImprimirResultado( $cliente, $cliente->Google_Videos());
		break;

	case 'listas_de_video':
		ImprimirResultado( $cliente, $cliente->Google_ListasDeVideo());
		break;

	case 'canales_de_video':
		ImprimirResultado( $cliente, $cliente->Google_CanalesDeVideo());
		break;

	case 'buscar_videos':
		$texto= @$_REQUEST['texto'];
		ImprimirResultado( $cliente, $cliente->Google_BuscarVideos($texto));
		break;

	case 'lideres':
		ImprimirResultado( $cliente, $cliente->FourSquare_Lideres());
		break;
	
	case 'amigos':
		ImprimirResultado( $cliente, $cliente->FourSquare_Amigos());
		break;

	case 'checkins':
		ImprimirResultado( $cliente, $cliente->FourSquare_Checkins());
		break;
	
	case 'fotos':
		ImprimirResultado( $cliente, $cliente->FourSquare_Fotos() );
		break;

	case 'lugares':
		ImprimirResultado( $cliente, $cliente->FourSquare_Lugares(@$_REQUEST['lat_lon']) );
		break;

	
	default:
	}
	echo '</div></div>';

}

function ImprimirResultado( ClienteOAuth $cliente, $resultado )
{

	if( $resultado  )
	{
		if( is_string($resultado) && $resultado[0]=='{'){
			$resultado = json_decode( $resultado, true);
		}
		if( is_array($resultado))
		{
			ob_start();
			print_r($resultado);
			$texto = ob_get_clean();
		}
		else
		{
			$texto = $resultado;
		}
		echo '<pre>',str_replace(array('<','>'), array('&lt;','&gt;'),$texto),'</pre>';
	}
	else
		echo 'Error: ',$cliente->GetUltimoError ();

}
function ImprimirEstadoServicio( ClienteOAuth $cliente, $servicio, $nombre )
{
?><div class="cuadro_aviso">
	<div class="titulo"><?php echo $nombre; ?></div>
	<div class="cuerpo"><?php

		if( ACCION_SOLICITADA && SERVICIO_SOLICITADO == $servicio )
		{
			ProcesarAccion( $cliente, $servicio, ACCION_SOLICITADA);
		}
		echo '<div>Estado: ';
		$token = $cliente->GetTokenDeAcceso($servicio);
		if( $token )
		{
			echo '<b style="color:#008000">Autorizado</b> [<a href="?accion=olvidar&servicio=',$servicio,'">olvidar</a>]';
		}
		else
		{
			echo '<b style="color:#c00000">No autorizado</b> [<a href="', $cliente->GetUrlParaAutorizarApp( $servicio ),'">autorizar</a>]';
		}
		echo '</div>';



		if( $token )
		{
			echo '<div><b>Comandos</b><ul>';
			ImprimirAcciones( $cliente, $servicio );
			echo '</ul></div>';
		}
	?></div>
</div><?php
}


function ImprimirAcciones( ClienteOAuth $cliente, $servicio )
{
	switch( $servicio )
	{
	case ClienteOAuth::svcGoogle:
		echo '<li><a href="?accion=videos&servicio=',$servicio,'">Videos</a></li>';
		echo '<li><a href="?accion=listas_de_video&servicio=',$servicio,'">Listas de videos</a></li>';
		echo '<li><a href="?accion=canales_de_video&servicio=',$servicio,'">Canales de videos</a></li>';
		echo '<li><a href="?accion=buscar_videos&servicio=',$servicio,'&texto=Paraguay">Buscar videos</a></li>';


		break;
	case ClienteOAuth::svcFourSquare:
		echo '<li><a href="?accion=lideres&servicio=',$servicio,'">Líderes</a></li>';
		echo '<li><a href="?accion=checkins&servicio=',$servicio,'">Checkins</a></li>';
		echo '<li><a href="?accion=amigos&servicio=',$servicio,'">Amigos</a></li>';
		echo '<li><a href="?accion=fotos&servicio=',$servicio,'">Fotos</a></li>';
		echo '<li><a href="?accion=lugares&servicio=',$servicio,'&lat_lon=-25.323871,-57.636257">Lugares</a></li>';
		
		break;
	}

}
?>