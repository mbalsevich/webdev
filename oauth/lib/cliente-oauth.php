<?php

class ClienteOAuth
{
	const 
		svcGoogle = 'google'
	,	svcTwitter= 'twitter'		// No habilitamos, usa oAuth 1.0
	,	svcFourSquare = 'foursquare'
	;
	private static
		$servicios= array(
			 self::svcGoogle => 'Google'
		   , self::svcFourSquare => 'Foursquare'
		 	);

	private static
		$config = array(
			self::svcGoogle=>array(
				'authURI'=>'https://accounts.google.com/o/oauth2/auth'
				,'tokenURI'=>'https://accounts.google.com/o/oauth2/token'
				,'redirectURI' => 'http://localhost/uca/oauth/google-cb.php'
				,'clientId' => '634000516038.apps.googleusercontent.com'
				,'clientSecret'=>'6vZFSJxrULv6C87lVmk5BG4t'
				,'apis'=>array(
					'youtube'=>'https://www.googleapis.com/auth/youtube'
				   ,'contactos'=>'https://www.google.com/m8/feeds/')
				)
		,self::svcFourSquare=>array(
				'authURI'=>'https://foursquare.com/oauth2/authorize'
				,'tokenURI'=>'https://foursquare.com/oauth2/access_token'
				,'redirectURI' => 'http://localhost/uca/oauth/foursquare-cb.php'
				,'clientId' => 'UEHKK1FEEA2VIHDYOUWXSUPAWXJ10CTU1GUBWJHPHJHOZ1ZB'
				,'clientSecret'=>'MBOIWMYIMQYIKDK2GWEL3LSBAT3XH5505RRO0ZEZOEEGMJ1T'
				,'apiVersion'=>'20130727'
				,'apis'=>array()
				)
		);

	private $ultimo_error=null;

	function __construct() {
		if( !session_id() ){
			session_start();
		}
	}

	function GetListaDeServicios() {
		return self::$servicios;
	}
	
	function GetNombreDelServicio($servicio){
		$nombre = @self::$servicios[$servicio];
		if( !$nombre ) return '?'.$servicio;
		return $nombre;
	}
	
	function GetUltimoError() {
		return $this->ultimo_error;
	}
	
	function GetConfiguracion( $servicio ){
		$cfg = @self::$config[$servicio];
		if( !$cfg ){
			$this->ultimo_error=null;
		} else {
			$this->ultimo_error= "Sin configuración para servicio '$servicio'";
		}
		return $cfg;
	}

	function GetTokenDeAcceso( $servicio ){
		return $this->LeerVar($servicio.'_token');
	}

	function OlvidarTokenDeAcceso($servicio) {
		return $this->SetDatosTokenDeAcceso($servicio, null );
	}
	
	/**
	 * GOOGLE
	 */

	

	function Google_Videos( $parametros=array('myRating'=>'like','part'=>'snippet') ){
		return $this->ApiGoogle(
				 'https://www.googleapis.com/youtube/v3/videos'
				, $parametros
				);
	}
	function Google_ListasDeVideo( $parametros=array('mine'=>'true','part'=>'snippet') ){
		return $this->ApiGoogle(
				 'https://www.googleapis.com/youtube/v3/playlists'
				, $parametros
				);
	}
	function Google_CanalesDeVideo( $parametros=array('mine'=>'true','part'=>'snippet') ){
	
		return $this->ApiGoogle(
				 'https://www.googleapis.com/youtube/v3/channels'
				, $parametros
				);
	}
	function Google_BuscarVideos( $texto='paraguay'
			,$parametros=array('type'=>'video','part'=>'snippet','maxResults'=>20) ){

		$parametros['q']=$texto;
		return $this->ApiGoogle(
				 'https://www.googleapis.com/youtube/v3/search'
				, $parametros
				);
	}

	/**
	 * FOURSQUARE
	 */

	function FourSquare_Lideres(){
		return $this->ApiFourSquare('https://api.foursquare.com/v2/users/leaderboard'
				, array()
				);
	}


	function FourSquare_Amigos($userId='self', $parametros=array()){
		return $this->ApiFourSquare('https://api.foursquare.com/v2/users/'.$userId.'/friends'
				, $parametros
				);
	}
	function FourSquare_Checkins($userId='self', $parametros=array()){
		return $this->ApiFourSquare('https://api.foursquare.com/v2/users/'.$userId.'/checkins'
				, $parametros
				);
	}
	function FourSquare_Fotos($userId='self', $parametros=array()){
		return $this->ApiFourSquare('https://api.foursquare.com/v2/users/'.$userId.'/photos'
				, $parametros
				);
	}
	
	function FourSquare_Lugares($latitud_longitud='-25.323871,-57.636257'
			, $parametros=array()){
		
		$parametros = array_merge(array('limit'=>20,'radius'=>500), $parametros );
		$parametros['ll']=$latitud_longitud;
		
		return $this->ApiFourSquare('https://api.foursquare.com/v2/venues/search'
				, $parametros
				);
	}

	/**
	 * LLAMADAS INTERNAS
	 */
	private function
	ApiGoogle($url,$parametros=array())
	{
		$servicio = self::svcGoogle;
		$cfg = $this->GetConfiguracion( $servicio );
		if( !$cfg ) {
			return null;
		}
		$token = $this->GetDatosTokenDeAcceso($servicio);
		if( !$token ){
			return null;
		}

		$authorizacion = $token['tipo'] . ' ' . $token['token'];
		$opciones = array(
			CURLOPT_HTTPHEADER=>array('Authorization: '.$authorizacion
				)
			);
		return $this->ApiGET( $url, $parametros, $opciones);
	}

	
	private function
	ApiFourSquare($url,$parametros=array())
	{
		$servicio = self::svcFourSquare;
		$cfg = $this->GetConfiguracion( $servicio );
		if( !$cfg ) {
			return null;
		}
		$token = $this->GetDatosTokenDeAcceso($servicio);
		if( !$token ){
			return null;
		}
		// Le que enviaremos
		$parametros['oauth_token']=$token['token'];
		$parametros['v']=$cfg['apiVersion'];
		
		$datos =  $this->ApiGET( $url, $parametros);
		if( !$datos ){
			return null;
		}
		if( ($json=@json_decode($datos,true)) == null
			|| !array_key_exists( 'response', $json ) )
		{
			$this->ultimo_error = 'Respuesta no comprendida: '.$datos;
			return null;
		}
		return $json['response'];
	}

	private function
	ApiGET( $url, $parametros=array() , $opciones=array())
	{
		$curl = curl_init();
		if( !$curl )
		{
			$this->ultimo_error='No puedo inicializar CURL';
			return null;
		}

		$respuesta = null;
		$this->ultimo_error = null;

		$url .= '?'.\http_build_query($parametros);

		// Juntar las opciones con aquellas enviadas
		$opciones[CURLOPT_URL]=$url;					// El URL
		$opciones[CURLOPT_HEADER]=false;				// No traer el header
		$opciones[CURLOPT_RETURNTRANSFER]=true;			// Retornar el resultado
		$opciones[CURLOPT_SSL_VERIFYPEER]=false;		// No verificar certificado SSL
		
		if( !curl_setopt_array($curl,$opciones) )
		{
			$this->ultimo_error = 'No puedo establecer las opciones de CURL: '.curl_error($curl);
		}
		else
		{
			$respuesta = curl_exec( $curl );
			if( !$respuesta  ){
				$this->ultimo_error = 'Error "'.curl_error($curl).'"';
			}
		}
		curl_close($curl); // Cerrar CURL
		return $respuesta;
		
	}

	private function GetDatosTokenDeAcceso( $servicio ){
		$datos = $this->LeerVar($servicio.'_datos');
		if( !$datos ){
			$this->ultimo_error='No hay token para el servicio '.$servicio;
		}
		return $datos;

	}

	private function SetDatosTokenDeAcceso( $servicio, $datos ){
		$this->EscribirVar($servicio.'_token',@$datos['token']);
		return $this->EscribirVar($servicio.'_datos', $datos);
	}

	function GetUrlParaAutorizarApp($servicio, $opciones=array())
	{
		$this->ultimo_error = null;

		$cfg = $this->GetConfiguracion($servicio);
		if( !$cfg ){
			return './?servicio='.$servicio.'&accion=error&error=Sin+datos+para+servicio';
		}

		// Juntar todas las apis con espacios en medio
		$scope = implode(' ',$cfg['apis']);

		$parametros = array(
			'client_id'=>$cfg['clientId']			// Nuestro ID
			,'redirect_uri'=>$cfg['redirectURI']	// Volver a esta URL
			,'response_type'=>'code'					// Queremos un código
			,'scope'=>$scope							// Lista de apis
		);
		if( @$opciones['preguntar_siempre'] || @$cfg['preguntar_siempre'])
		{
			// Mostrar siempre la pregunta al usuario
			$parametros['approval_prompt']='force';
		}
		return $cfg['authURI'].'?'.\http_build_query($parametros);
	}


	function ObtenerTokenDeAcceso($servicio, $codigo_autorizacion){
		$cfg = $this->GetConfiguracion($servicio);
		if( !$cfg ){
			return false;
		}

		$this->OlvidarTokenDeAcceso($servicio);

		$curl = curl_init();
		if( !$curl )
		{
			$this->ultimo_error='No puedo inicializar CURL';
			return false;
		}

		$this->ultimo_error = null;
		// Le que enviaremos
		$parametros = array(
			 'client_id'=>$cfg['clientId']			// Nuestro ID
			,'client_secret'=>$cfg['clientSecret']	// El "Secreto"
			,'redirect_uri'=>$cfg['redirectURI']	// Volver a esta URL
			,'grant_type'=>'authorization_code'			// Enviamos un código
			,'code'=>$codigo_autorizacion				// El código
			,'scope'=>''								// Mismo ccope del codigo
		);

		$opciones = array(
			  CURLOPT_URL=>$cfg['tokenURI']			// El URL
			, CURLOPT_HEADER=>false					// No traer el header
			, CURLOPT_RETURNTRANSFER => true		// Retornar el resultado
			, CURLOPT_POST=>true					// Enviar con POST
			, CURLOPT_POSTFIELDS=>$parametros		// Los parámetros
			, CURLOPT_SSL_VERIFYPEER=>false
		  );

		if( !curl_setopt_array($curl,$opciones) )
		{
			$this->ultimo_error = 'No puedo establecer las opciones de CURL: '.curl_error($curl);
		}
		else
		{
			$respuesta = curl_exec( $curl );
			if( !$respuesta )
			{
				$this->ultimo_error = 'Error al leer datos ('.curl_error($curl).')';
			}
			else
			{
				$datos = $this->ProcesarRespuestaToken( $servicio, $respuesta );
			}
		}
		curl_close($curl); // Cerrar CURL

		if( $datos )
		{
			$this->SetDatosTokenDeAcceso($servicio, $datos );
			return true;
		}

		return false;
	}

	private function ProcesarRespuestaToken($servicio, $respuesta )
	{
		$this->ultimo_error = null;
		switch( $servicio )
		{
		case self::svcGoogle:
		case self::svcFourSquare:
			if( ($json = @json_decode($respuesta,true))==null
			|| !is_array($json))
			{
				$this->ultimo_error = 'Respuesta del servidor inválida';
				break;
			}
			$token = @$json['access_token'];
			if( !$token)
			{
				$this->ultimo_error = 'Servidor no nos dió un token';
				$error = @$json['error'];
				if( $error )$this->ultimo_error.= ', dijo: "'.$error.'"';
				break;
			}
			return array('token'=>$token
					,'tipo'=>@$json['token_type']
					,'ttl'=>@$json['expires_in']);


		default:
			$this->ultimo_error = "No sé como procesar la respuesta del servicio '$servicio'";
			break;
		}
		return null;
	}

	private function LeerVar( $nombre ) {
		return @$_SESSION[ $nombre ];
	}
	private function EscribirVar( $nombre , $valor ) {
		@$_SESSION[ $nombre ] = $valor;
		return true;
	}

	static function log($obj,$titulo=null){

		if( is_array($obj) || is_object($obj)) {
			ob_start();
			print_r($obj);
			$texto = ob_get_clean();
		} else {
			$texto = $obj;
		}
		@error_log( $titulo ? "$titulo: $texto\n" : $texto."\n", 3, 'oauth.log');
	}
	
}

?>